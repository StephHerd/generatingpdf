
#!/usr/bin/python
# -*- coding: utf-8 -*-
#this imports need revisiting as we might not need most of them! depends
#on the complexity of the PDF PT wants
from django.utils.translation import ugettext_lazy as _
# from reportlab.graphics.widgets.markers import makeMarker
# from reportlab.graphics.charts.barcharts import VerticalBarChart
# from reportlab.graphics.charts.legends import Legend
# from reportlab.graphics.charts.linecharts import SampleHorizontalLineChart
# from reportlab.graphics.charts.piecharts import Pie
# from reportlab.graphics.charts.textlabels import Label
# from reportlab.graphics.shapes import Drawing
# from reportlab.lib import colors
# from reportlab.lib.enums import TA_CENTER, TA_JUSTIFY
from reportlab.lib.pagesizes import letter, A4
# from reportlab.lib.units import mm
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
# from reportlab.pdfbase import pdfmetrics
# from reportlab.pdfbase.ttfonts import TTFont
import os
import urllib2
from reportlab.lib.pagesizes import letter
from reportlab.platypus import SimpleDocTemplate, Image
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Table,\
    TableStyle


class PdfPrint:

    # initialize class
    def __init__(self, buffer, pageSize):
        self.buffer = buffer
        # default format is A4
        if pageSize == 'A4':
            self.pageSize = A4
        elif pageSize == 'Letter':
            self.pageSize = letter
        self.width, self.height = self.pageSize

    def pageNumber(self, canvas, doc):
        number = canvas.getPageNumber()
        canvas.drawCentredString(100*mm, 15*mm, str(number))

    def report(self, dataToPdf, title):
        # set some characteristics for pdf document
        filename = 'polls/pt.png'
        doc = SimpleDocTemplate(
            self.buffer,
            rightMargin=72,
            leftMargin=72,
            topMargin=30,
            bottomMargin=72,
            pagesize=self.pageSize)
        styles = getSampleStyleSheet()

        # create document
        data = []
        data.append(Paragraph(title, styles['Title']))
        data.append(Paragraph(u'{0}'.format(dataToPdf), styles['Title']))
        data.append(Spacer(1, 12))
        data.append(Image(filename))

        data.append(Spacer(1, 48))
        # create other flowables
        doc.build(data)
        pdf = self.buffer.getvalue()
        self.buffer.close()
        return pdf
