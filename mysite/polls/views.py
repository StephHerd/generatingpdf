from django.shortcuts import render
from reportlab.pdfgen import canvas
from django.http import HttpResponse
from io import BytesIO
from .models import Question
from django.template import loader
from .pdf_utils import PdfPrint
from datetime import date

# Create your views here.



# def index(request):
#     print 'xxxx'
#     latest_question_list = Question.objects.order_by('-pub_date')[:5]
#     template = loader.get_template('polls/index.html')
#     context = {
#         'latest_question_list': latest_question_list,
#     }
#     return HttpResponse(template.render(context, request))


def detail(request, question_id):
    return HttpResponse("You're looking at question %s." % question_id)

def results(request, question_id):
    response = "You're looking at the results of question %s."
    return HttpResponse(response % question_id)

def vote(request, question_id):
    return HttpResponse("You're voting on question %s." % question_id)

def create_pdf (request):
    #dataToPdf = Question.objects.all()
    dataToPdf = 'PT QUESTIONS HERD'
    response = HttpResponse(content_type='application/pdf')
    today = date.today()
    filename = 'pdf_demo' + today.strftime('%Y-%m-%d')
    response['Content-Disposition'] = 'attachement; filename={0}.pdf'.format(filename)
    buffer = BytesIO()
    report = PdfPrint(buffer, 'A4')
    pdf = report.report(dataToPdf, 'Princes Trust Feedback')
    response.write(pdf)
    return response
